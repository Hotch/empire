using Empire.Data.Emuns;
namespace Empire.Data
{
    public class Church : Building, IBuilding<Monk>
    {
        public Church()
        {
            Type = BuildingType.Church;
        }
        public Monk CreateHuman()
        {
            return new Monk();
        }

    }
}