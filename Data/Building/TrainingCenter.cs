using Empire.Data.Emuns;
namespace Empire.Data
{
    public class TrainingCenter : Building, IBuilding<Knight>
    {
        public TrainingCenter()
        {
            Type = BuildingType.TrainingCenter;
        }
        public Knight CreateHuman()
        {
            return new Knight();
        }
    }
}