using Empire.Data.Emuns;
namespace Empire.Data
{
    public class Farmhouse : Building, IBuilding<Farmer>
    {
        public Farmhouse()
        {
            Type = BuildingType.Farmhouse;
        }
        public Farmer CreateHuman()
        {
            return new Farmer();
        }

    }
}