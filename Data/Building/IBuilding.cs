namespace Empire.Data {
    public interface IBuilding<T> {
        T CreateHuman ();
    }
}