using System;
using Empire.Data.Emuns;
namespace Empire.Data
{
    public class Building
    {
        public Building()
        {
            Id = Guid.NewGuid();
        }
        public Guid Id { get; set; }
        public BuildingType Type { get; set; }

    }
}