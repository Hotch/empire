using System.Collections.Generic;
using System.Linq;
using Empire.Data.Emuns;
namespace Empire.Data
{
    public class Corps
    {

        private List<Human> _human = new List<Human>();
        private Farmer _DefaultFarmer = new Farmer();
        public Corps()
        {
            _human.Add(_DefaultFarmer);
            Human = _human;
            Building = new List<Building>();
        }
        public string Id { get; set; }

        public List<Human> Human { get; set; }

        public List<Building> Building { get; set; }

        public Farmer DefaultHuman => _DefaultFarmer;

    }
}