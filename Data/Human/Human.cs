using System;
using Empire.Data.Emuns;
namespace Empire.Data
{
    public class Human
    {
        public Human()
        {
            Id = Guid.NewGuid();

        }
        public Guid Id { get; set; }
        public Career Career { get; set; }

    }
}