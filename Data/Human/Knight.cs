using Empire.Data.Emuns;
namespace Empire.Data
{
    public class Knight : Human
    {
        public Knight()
        {
            Career = Career.Knight;
        }
    }
}