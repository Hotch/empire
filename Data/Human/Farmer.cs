using System;
using Empire.Data.Emuns;
namespace Empire.Data
{
    public class Farmer : Human
    {

        public Farmer()
        {
            Career = Career.Farmer;
        }

        public T CreateBuilding<T>()
        {

            return (T)Activator.CreateInstance(typeof(T));
        }
    }
}