namespace Empire.Data.Emuns
{
    public enum BuildingType
    {
        Church,
        Farmhouse,
        TrainingCenter
    }
}