﻿using System;
using System.Linq;
using Empire.Data;
using Empire.Data.Emuns;
namespace Empire
{
    class Program
    {
        static void Main(string[] args)
        {
            var corps = new Corps();

            var church = corps.DefaultHuman.CreateBuilding<Church>();
            var farmhouse = corps.DefaultHuman.CreateBuilding<Farmhouse>();
            var trainingCenter = corps.DefaultHuman.CreateBuilding<TrainingCenter>();

            corps.Building.Add(church);
            corps.Building.Add(farmhouse);
            corps.Building.Add(trainingCenter);

            corps.Human.Add(church.CreateHuman());
            corps.Human.Add(church.CreateHuman());
            corps.Human.Add(church.CreateHuman());
            corps.Human.Add(farmhouse.CreateHuman());
            corps.Human.Add(trainingCenter.CreateHuman());

            string output = string.Format("軍團一共有 {0}個農民 {1}個僧侶 {2}個騎士 {3}棟農舍 {4}棟教堂 {5}棟訓練所",
                corps.Human.Where(x => x.Career == Career.Farmer).ToList().Count.ToString(),
                corps.Human.Where(x => x.Career == Career.Monk).ToList().Count.ToString(),
                corps.Human.Where(x => x.Career == Career.Knight).ToList().Count.ToString(),
                corps.Building.Where(x => x.Type == BuildingType.Farmhouse).ToList().Count.ToString(),
                corps.Building.Where(x => x.Type == BuildingType.Church).ToList().Count.ToString(),
                corps.Building.Where(x => x.Type == BuildingType.TrainingCenter).ToList().Count.ToString()
            );

            Console.WriteLine(output);
            Console.Read();
        }
    }
}